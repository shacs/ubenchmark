import subprocess
import re

def disassemble(executable, allFuncs, nameMapping):
	try:
		p = subprocess.Popen(["objdump", "-d", "--no-show-raw-insn", executable], 
			stdout = subprocess.PIPE, stderr = subprocess.PIPE)
		stdout, _ = p.communicate()
	except OSError as e:
		print("Execution Dissasemble Error:", str(e))
	#print "STDOUT:"+str(stdout)
	
	myFunctions = stdout.replace("::", "")
	myFunctions = myFunctions.split("Disassembly of section .text:\n")[1]
	myFunctions = myFunctions.split("Disassembly of section .fini:\n")[0]

	functionNames = re.findall(r'\S* <.*>:', myFunctions)
	
	for func in functionNames:
		func = " ".join(func.split(" ")[1:])
		func = re.sub(r'[<|>|:]','', func)
		myBody = myFunctions.split(" <"+func+">:\n")
		if len(myBody) <= 1:
			print "Body for:"+str(func) + " unavailable"
			continue
		bodyLines = myBody[1].split("\n\n")[0].split("\n")
		if func not in nameMapping:
			continue
		for line in bodyLines:
			addr, inst = line.split(":\t")
			allFuncs[nameMapping[func]].addLineObjDump(str(addr).strip(), str(inst))
			#print "Addr:"+str(addr).strip()+ " -- inst:"+str(inst)
	return allFuncs

#Incase I use it later on
def demangle(name):
	try:
		p = subprocess.Popen(["c++filt", name], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
                #This waits for the process to finish before proceeding
		stdout, _ = p.communicate()
		demangled = stdout.split("\n")
		return demangled[0]
	except OSError as e:
		print("Execution failed:", e)
