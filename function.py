class Function():
	def __init__(self, ID, name, pointer, calls):
		self.ID = ID
		self.name = name
		self.pointer = pointer
		self.calls = calls
		self.linesValgrind = {}
		self.linesObjDump = {}

	def addCalls(self, calls):
		self.calls += calls

	def addLineValgrind(self, address, noIdea, execCount):
		(self.linesValgrind)[address] = {'count':execCount}

	def addLineObjDump(self, address, inst):
		(self.linesObjDump)["0x"+address] =  {'inst':inst}
