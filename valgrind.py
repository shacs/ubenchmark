from common import randomString
from function import Function
import subprocess

import sys
import re
import os

def simulate(executable):
	#return "tempFiles/temp_qik7k.txt"
	#File to export the valgrind simulation
	#Chose to expand to file in case I want to debug, the file will be available instead of print
	tempFile = "tempFiles/temp_"+str(randomString(5))+".txt"
	#Execute valgrind to dump data with instruction count of each assembly line
	try:
		os.system("valgrind --tool=callgrind --combine-dumps=yes --dump-instr=yes --demangle=no --compress-pos=no --callgrind-out-file="+tempFile + " " +executable)
		#This waits for the process to finish before proceeding
	except OSError as e:
		print("Execution failed:", e)

	return tempFile

def analyse(fileName):
	with open(fileName, 'r') as f:
		output = f.read()
	functions, nameMapping = catchFunctions(output)
	#for func in functions:
	#	print "Func:"+str(functions[func].name)
	return functions, nameMapping
	
def catchFunctions(data):
	allFuncs = {}
	nameIDMapping = {}
	
	#Captures functions that at their 1st declaration
	funcs = re.findall(r'cfn=\(\S*\) .*\ncalls=.*\n', data)

	for func in funcs:
		#It is 2 lines in valgrind output. 1st line for function name and next for calls
		lines = func.split("\n")
		#Find function ID and name
		myID = re.findall(r'\(\d*\)', lines[0])[0]
		myID = re.sub(r'[\(|\)]','', myID)
		#myName = (re.findall(r' \S*', lines[0])[0]).replace(" ","")
		myName = " ".join(lines[0].split(" ")[1:])
		#Find function calls count, x86 address and some number I need TODO investigate
		temp = lines[1].replace("calls=","").split(" ")
		if myID not in allFuncs:
			allFuncs[myID] = Function(myID, myName, temp[1], int(temp[0]))
			nameIDMapping[myName]= str(myID)
		else:
			print "Warning: Shouldn't have 2 declarations for same function:%s in valgrind.py" % (myName)
			allFuncs[myID].addCalls(int(temp[0]))
	
	#Captures functions in their 2nd and on calls
	funcs = re.findall(r'cfn=\(\S*\)\ncalls=.*\n', data)

	for func in funcs:
		lines = func.split("\n")
		myID = re.findall(r'\(\d*\)', lines[0])[0]
		myID = re.sub(r'[\(|\)]','', myID)
		temp = lines[1].replace("calls=","").split(" ")
		if myID not in allFuncs:
			allFuncs[myID] = Function(myID, "**NAMELESS**", temp[1], int(temp[0]))
		else:
			allFuncs[myID].addCalls(int(temp[0]))
	
	funcBodies = re.compile("\nfn=").split(data)

	for body in funcBodies:
		#print "Body:"+body
		lines = re.findall(r'0x\S* \d+ \d+', body)
		myFunc= (body.split("\n")[0]).split(" ")[0].replace("(","").replace(")","")
		for line in lines:
			add, noIdea, execCount = line.split(" ")
			if myFunc in allFuncs and "calls" not in line:
				allFuncs[myFunc].addLineValgrind(add, noIdea, execCount)

	#for func in allFuncs:
	#	if "dist" in str(allFuncs[func].name):
	#		print "Name:%s Count:%s" % (allFuncs[func].name, allFuncs[func].calls)
	#		print str(allFuncs[func].linesValgrind)

	return allFuncs, nameIDMapping
