import string, random

#Get  a random string of default length 4
def randomString(length = 4):
	options = string.ascii_uppercase + string.digits + string.ascii_lowercase
	return ''.join(random.choice(options) for _ in range(length))
