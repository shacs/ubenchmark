from collections import OrderedDict
from function import Function

def extract(functions):
	allLoops = []
	for func in functions:
		calls = functions[func].calls
		loopMode = False
		loopCount = 1
		currentLoop = None
		for line in OrderedDict(sorted(functions[func].linesObjDump.items())):
			try:
				ECR = round((1.0*int(functions[func].linesObjDump[line]['count']))/int(calls))
				#print "Function calls:%s, lineExec:%s, ECR:%s, loopCount:%s, loopMode:%s" %(calls, functions[func].linesObjDump[line]['count'], ECR, loopCount, loopMode)
				#Previous instructions were loops and current stopped
				if ECR <= 1 and not loopMode:
					continue
				if ECR <= 1 and loopMode:
					loopMode = False
					if len(currentLoop.inst) > 1:
						allLoops.append(currentLoop)
					continue
				#New Loop starting
				if ((ECR > 1) and (loopCount != ECR)):
					loopMode = True
					loopCount = ECR
					currentLoop = Loop(functions[func].name, ECR)
				if loopMode:
					currentLoop.addLine(functions[func].linesObjDump[line]['inst'])
			except Exception as e:
				#print "Exception loop.extract:"+str(e)
				pass
	#for loop in allLoops:
	#	print "Function:"+str(loop.functionName)+" ECR:"+str(loop.executionCount)
	#	for line in loop.inst:
	#		print "\tInst:"+str(line)
	return allLoops

class Loop():
	def __init__(self, functionName, executionCount):
		self.functionName = functionName
		self.executionCount = executionCount
		self.inst = []
	
	def addLine(self, inst):
		(self.inst).append(inst)

