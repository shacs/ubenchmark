#My Files
import valgrind
import objdump
import matching
import loop
import buildWorkload

#System Files
import argparse
import os

def main():
	parser = argparse.ArgumentParser(description="Isolate loops and join in a single workload if instructed.")
	parser.add_argument('-exc', metavar="exc", help='Workload Executable to test')
	args = parser.parse_args()
	if args.exc is None:
		parser.print_help()
		return
	execSplit = args.exc.split(" ")
	executable = execSplit[0]
	parameters = ""
	if len(execSplit) > 1:
		parameters = " ".join(execSplit[1:])
	print "Execute:"+str(executable)

	if not (executable).startswith("./"):
		print "FIX: Adding: './' to executable"
		executable = "./"+executable
	elic(executable, parameters)

#Given an executable, orchestrates the tool execution path
def elic(executable, parameters):
	try:
		print "Simulating with valgrind..."
		valgrindFile = valgrind.simulate(executable + " " + parameters)
		print "Analysing valgrind output..."
		functions, nameIDMapping = valgrind.analyse(valgrindFile)
		print "Assembling..."
		functionsASM = objdump.disassemble(executable, functions, nameIDMapping)
		print "Matching simulations together..."
		functionsMatched = matching.ASMtoCount(functionsASM)
		print "Collecting loops..."
		allLoops = loop.extract(functionsMatched)
		print "Building Mini-Workload..."
		buildWorkload.export(allLoops)
	except Exception as e:
		print "Error encountered at:"+str(e)
	finally:
		print "Deleting:"+valgrindFile
		os.remove(valgrindFile)

if __name__ == "__main__":
	main()
