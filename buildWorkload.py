def export(loops):
	try:
		count = 0
		with open("output.cpp", "w") as f:
			allCalls = ""
			f.write("#include <stdio.h>\n\n")
			for index, loop in enumerate(loops):
				print "%s) Loop:%s" % (index, loop)
				allCalls += "\n\tcall"+str(index)+"();"
				f.write("void call"+str(index)+"""(){\n\t__asm__(\n\t\t"mov $0, %ecx;"\n\t\t".L""" + str(count)+""":"\n""")
				for line in loop.inst:
					f.write("\t\t\""+str(line)+";\"\n")
		
				f.write("""\t\t"addl   $0x1, %ecx;"\n\t\t"cmp    $""" + str(hex(int(loop.executionCount)))+ """,%ecx;"\n\t\t"jle    .L"""+ str(count)+""";");\n\t}\n""")
				count += 1
			f.write("int main(){")
			f.write(allCalls)
			f.write("\n\treturn 0;\n}")
	except Exception as e:
		print "Exception in Export Loop:"+str(e)
