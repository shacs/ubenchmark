from collections import OrderedDict

def ASMtoCount(functions):
	for func in functions:
		if len(functions[func].linesObjDump) == 0:
			continue
		for line in OrderedDict(sorted(functions[func].linesObjDump.items())):
			myCount = "N/A"
			NACount = 0
			try:
				functions[func].linesObjDump[line]['count'] = functions[func].linesValgrind[line]['count']
			except:
				NACount += 1
	return functions
